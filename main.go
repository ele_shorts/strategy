package main

import (
	"fmt"
	"sort"
)

type (
	Sort interface {
		Run(a []int)
	}
	bubbleSort   struct{}
	standardSort struct{}
)

func (b *bubbleSort) Run(a []int) {
	fmt.Println("bubble")
	len := len(a)
	for i := 0; i < len-1; i++ {
		for j := 0; j < len-i-1; j++ {
			if a[j] > a[j+1] {
				a[j], a[j+1] = a[j+1], a[j]
			}
		}
	}
}
func (s *standardSort) Run(a []int) {
	fmt.Println("standard")
	sort.SliceStable(a, func(i, j int) bool {
		return a[i] < a[j]
	})
}

type (
	F struct {
		Elements []int
		Sort     Sort
	}
)

func InitF(s Sort) *F {
	return &F{
		Elements: make([]int, 0),
		Sort:     s,
	}
}

func (f *F) Show() {
	fmt.Println(f.Elements)
}

func (f *F) Add(i ...int) {
	f.Elements = append(f.Elements, i...)
}
func (f *F) Order() {
	f.Sort.Run(f.Elements)
}

func (f *F) SetSort(s Sort) {
	f.Sort = s
}

func main() {
	f := InitF(&bubbleSort{})
	f.Add(19, 29, 0, -123)
	f.Order()
	f.Show()

	f.SetSort(&standardSort{})
	f.Add(184, 19, -23989)
	f.Order()
	f.Show()
}
